from persona import Persona
from gpio import Gpio

print("Hola Mundo!")

p = Persona("Juan", 10)
q = Persona("Pedro", 20)

personas = []

personas.append(p)
personas.append(q)

print(p.es_mayor_de_edad())
print(Persona.get_mayor(personas))

Persona.print_personas(personas)
Persona.dump_csv("personas.csv", personas)
personas = Persona.load_csv("personas.csv")
print("Cargado de .csv")
for persona in personas:
    print(persona)


gpio5 = Gpio(5)

gpio5.set_state(True)
print(gpio5.get_state())
gpio5.set_state(False)
print(gpio5.get_state())


