class Gpio:

    def __init__(self, gpio_number):
        self.__gpio_number = gpio_number

    def set_state(self, state):
        file_name = f"/tmp/gpio_{self.__gpio_number}.data"
        with open(file_name, "w+", encoding="utf-8") as file:
            if state:
                file.write("1")
            else:
                file.write("0")
    
    def get_state(self):
        file_name = f"/tmp/gpio_{self.__gpio_number}.data"
        with open(file_name, "r", encoding="utf-8") as file:
            if file.read() == "1":
                return True
            else:
                return False