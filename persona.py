class Persona:

    __MAYORIA_EDAD = 18
    
    def __init__(self, nombre, edad):
        self.set_nombre(nombre)
        self.set_edad(edad)

    def set_nombre(self, nombre):
        if nombre != "":
            self.__nombre = nombre

    def set_edad(self, edad):
        self.__edad = edad

    def get_nombre(self):
        return self.__nombre

    def get_edad(self):
        return self.__edad

    def __str__(self):
        return "Nombre: {}\nEdad: {}".format(self.get_nombre(), self.get_edad())

    def es_mayor_de_edad(self):
        return self.get_edad() >= Persona.__MAYORIA_EDAD

    def es_mayor_que(self, persona):
        return self.get_edad > persona.get_edad() 

    @staticmethod
    def get_mayor(personas):
        mayor_edad = 0
        for persona in personas:
            if persona.get_edad() > mayor_edad:
                mayor_edad = persona.get_edad()
        return mayor_edad

    @staticmethod
    def print_personas(personas):
        for persona in personas:
            print(type(persona))
            print("{} {}".format(persona.get_nombre(), persona.get_edad()))

    @staticmethod
    def dump_csv(file_name, personas):
        with open(file_name, "w+", encoding="utf-8") as file:
            header = "nombre,edad\n"
            print(header)
            file.write(header)
            for persona in personas:
                row = "{},{}\n".format(persona.get_nombre(), persona.get_edad())
                print(row)
                file.write(row)
            print("{} {}".format(persona.get_nombre(), persona.get_edad()))

    @staticmethod
    def load_csv(file_name):
        with open(file_name, "r", encoding="utf-8") as file:
            file.readline() #Descarta el header
            personas = []
            rows = file.readlines()
            for row in rows:
                fields = row.split(",")
                nombre = fields[0]
                edad = fields [1]
                personas.append(Persona(nombre, edad))
            return personas
